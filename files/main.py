from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from sklearn import svm
from sklearn import datasets
from joblib import dump, load
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression 


import pickle
import os

# creating a FastAPI server
server = FastAPI(title='User API')
class DataX(BaseModel):

    month: int
    clear: float
    cloudy: float
    rainy: float
    snowy: float
    hum_mean: float
    windspeed_mean: float
    temp_mean: float
    atemp_mean: float

@server.get('/dumpLinearRegression')
def dumpLinearRegression():
    """ Dump a linear regression  of the model
    """

    bike_csv= pd.read_csv('https://assets-datascientest.s3-eu-west-1.amazonaws.com/de/total/bike.csv')
    bike_by_quantive_value = bike_csv.groupby("dteday")[["hum","windspeed","temp","atemp"]].agg(["mean"])
    bike_by_quantive_value.columns = bike_by_quantive_value.columns.map('_'.join).str.strip('|')

    bikes = bike_csv
    # Agrégation par jour du nombre de vélos loués
    bike_count_day= bikes.groupby("dteday").agg({"cnt":"sum"})

    # Agrégation des lignes par situation météo quotidienne (avec arrondi)
    groupped_bike = bikes.groupby(["dteday","weathersit"]).agg({"cnt":"sum"})

    groupped_bike["%"] = groupped_bike.groupby(level=0).apply(
        lambda x:  x / x.sum()
    )
        # Pivotage de l'agrégat par situation météo quotidienne
    groupped_bike_reset_index = groupped_bike.reset_index()
    groupped_bike_pivot = groupped_bike_reset_index.pivot(index="dteday", columns="weathersit", values="%")
    groupped_bike_pivot = groupped_bike_pivot.fillna(0)


    # Jointure des deux groupes de données reformatées (catégorielles et numériques)
    bike_partiel  = groupped_bike_pivot.merge(bike_by_quantive_value, on="dteday", how="left")
    bike_final = bike_partiel.merge(bike_count_day, on="dteday", how="left")

    bike_final["target"] = bike_final["cnt"].shift(-1)

    bike_final

        # Définition du modèle

    bike_final_minus_1 = bike_final[:-1]
    bike_x = bike_final_minus_1.drop(['cnt', 'target'], axis=1)
    bike_y = bike_final_minus_1['target']
    print("bike_y", bike_y.isna().sum())

    X_train, X_test, y_train, y_test = train_test_split(bike_x, bike_y, test_size = 0.30, random_state = 42)
    print(X_train.shape,X_test.shape,y_train.shape,y_test.shape)
    # A partir du module linear_model de la librairie scikit learn on importe la fonction LinearRegression

    model_lin=LinearRegression().fit(X_train,y_train)

    # On prédit les y à partir de X_test

    y_pred=model_lin.predict(X_test)
    # On prédit les y à partir de X_train

    y_pred_train=model_lin.predict(X_train)
    # On affiche les coefficients obtenus

    dump(model_lin, 'bike.joblib')
    return "Dump du model lineaire effectué avec succés"



@server.get('/bikes/')
async def get_prediction(data:DataX):
    """Returns the prediction based on the specified model 
    """
    cwd = os.getcwd()
    print(cwd)
    dumpLinearRegression()

    clf = load('bike.joblib') 
    data = {'clear':[data.clear],
    'cloudy':[data.cloudy],
    'rainy':[data.rainy],
    'snowy':[data.snowy],
    'hum_mean':[data.hum_mean],
    'windspeed_mean':[data.windspeed_mean],
    'temp_mean':[data.temp_mean],
    'atemp_mean':[data.atemp_mean]}
    df = pd.DataFrame(data)
    print(df)

    predict = clf.predict(df)
    return predict[0]

